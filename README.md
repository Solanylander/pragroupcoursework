An application designed for teachers to allow them to read and edit studentís University records.

Pulls data from a server off-site and stored it inside files on the computer.

Then allow you to input student exam results and other information.

Was completed as a piece of coursework for which the student records server has now been taken offline.

Therefore application can no longer pull the records.

Written in JAVA