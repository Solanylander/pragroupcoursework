
public class student {
	
		private String name;
		private int number;
		private String email;
		private String tutor;
		private String markingcodes;
		private String results;
	public student(String n, int x, String e, String z){
		name = n;
		number = x;
		email = e;
		tutor = z;
		markingcodes = null;
		results = "";
	}
	
	public String getname(){
		String ret = name;
		return ret;
	}
	
	public int getnumber(){
		int ret = number;
		return ret;
	}
	
	public String getemail(){
		String ret = email;
		return ret;
	}
	
	public String gettutor(){
		String ret = tutor;
		return ret;
	}
	
	public String getmark(){
		String ret = markingcodes;
		return ret;
	}
	
	public void setmark(String mark){
		markingcodes = mark;
	}
	public void addgrade(String a, String b, String c){
		results += a+" "+b+":  "+c+"\n";
	}
	public String getresults(){
		return results;
	}

}
