import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;


public class main implements ActionListener {
	   private static JTextField TextInput;
	   static Students group;
	   static Students groupinscroll = new Students();
	   static JComboBox<String> JCstudents;
	   static boolean working = false;
	   public static void main(String[] args) {
		   final TabbedPane tabbedPane = new TabbedPane();
		     buildframe temp = new buildframe();
		     final JFrame frame = temp.getframe();
	    	 JMenuBar bar = new JMenuBar();
	    	 group = new Students();
	         JLabel JLtop = new JLabel("PRA Coursework - Alpha Evolution");
	           frame.setJMenuBar(bar);
	           frame.setLayout(new BorderLayout());
	           JCstudents = new JComboBox<>( );
	           frame.pack();
	           frame.setSize(640 ,480);
	       	JPanel west = new JPanel();
	       	frame.setLayout(new BorderLayout());
	    	frame.add(west, BorderLayout.WEST);
	    	west.setLayout( new BoxLayout(west, BoxLayout.PAGE_AXIS));
	    	TextInput = new JTextField();
	    	TextInput.getDocument().addDocumentListener(CSVreader);
	    	JMenu File = new JMenu("File");
	    	final JMenuItem LAMC = new JMenuItem("Load anonymous marking codes");
	    	final JMenuItem LER = new JMenuItem("Load exam results");
	    	bar.add(File);
	        bar.add(JLtop);
	    	File.add(LAMC);
	    	File.add(LER);

	    	 west.setMaximumSize(new Dimension(100,100));
	    	 TextInput.setMaximumSize(new Dimension(200,100));
	    	 JCstudents.setMaximumSize(new Dimension(200,100));
	    	 west.add(TextInput);
		     west.add(JCstudents);
		     west.add(Box.createRigidArea(new Dimension(10,500)));
		     TestStudentData g = new TestStudentData();
		     group = g.Test();
		     groupinscroll = g.Test();
		     int size = group.getsize();
	         for(int i = 0; i < size; i++){
	         student test = group.getstudent(i);
	         String addtest = test.getname() + " (" + test.getnumber() + ")";
	         JCstudents.addItem(addtest);
	         }
	         
		LAMC.addActionListener(new ActionListener() {

		    public void actionPerformed(ActionEvent e) {
		        Read obj = new Read();
		        obj.run(group);
		    }
		});
		LER.addActionListener(new ActionListener() {
			TabbedPane mainFrame	= new TabbedPane();
		    public void actionPerformed(ActionEvent e) {
		    	try{
		        ExamResults obj = new ExamResults();
		        Assessment assess = obj.run(group);
		        for(int i = 0; i < assess.getResults().size(); i++){
		        	String a = assess.getResults().get(i).getcand().substring(1);
		        	for(int j = 0; j < group.getsize(); j++){
		        		String b = group.getstudent(j).getmark();
		        			if(a.equals(b)){
		        				assess.getResults().get(i).setcand(group.getstudent(i).getnumber()+"");

				        		group.getstudent(j).addgrade(assess.getResults().get(i).getmodule(), assess.getResults().get(i).getass(), assess.getResults().get(i).getmark());
		        				}
		        	}
		        	if(assess.getResults().get(i).getcand().contains("/")){
		        		int p = 1;
		        		String temp = "";
		        		String move = assess.getResults().get(i).getcand();
		        		while (!((move.charAt(p)+"").equals("/"))){
		        			temp += move.charAt(p);
		        			p++;

				    	
		        		}
		        		assess.getResults().get(i).setcand(temp);
		        		for(int j = 0; j < group.getsize(); j++){
		        			if (temp.equals(group.getstudent(j).getnumber()+"")){
		        		group.getstudent(j).addgrade(assess.getResults().get(i).getmodule(), assess.getResults().get(i).getass(), assess.getResults().get(i).getmark());
		        			}
		        		}
		        	}
		        	}

			if ((assess.getResults().get(0).getass().charAt(0)+"").equals("#")){
				mainFrame.setVisible( true );
				mainFrame.newPage(assess,group);
		    	}
		    	}
		    	catch(Exception e1){

		    	}        
		    	groupsearch();
		    }
		});
	    
		JCstudents.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				informationpanel info = new informationpanel(working, groupinscroll, JCstudents);	
			}
		});
		
		TextInput.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				informationpanel info = new informationpanel(working, groupinscroll, JCstudents);
			}
		});
  }
	   

	   
	   static DocumentListener CSVreader = new DocumentListener() {
		   
		public void changedUpdate(DocumentEvent arg0) {
			
		}

		public void insertUpdate(DocumentEvent arg0) {
			 groupsearch();
		}

		public void removeUpdate(DocumentEvent arg0) {
			 groupsearch();
		}
		   
	};
	   
	public void actionPerformed(ActionEvent arg0) {
		
	}

	public static void groupsearch() {
		String s = TextInput.getText();
		working = true;
		JCstudents.removeAllItems();
		groupinscroll.clear();
        	for(int i = 0; i < group.getsize(); i++){
        		student test = group.getstudent(i);
        		String t = test.getnumber() + "";
        		if(test.getname().toLowerCase().contains(s.toLowerCase()) || t.contains(s)){
        			String addtest = test.getname() + " (" + test.getnumber() + ")";
        			JCstudents.addItem(addtest);
        			groupinscroll.addstudent(test);
        		}
        	}
        working = false;
	}
}

 
