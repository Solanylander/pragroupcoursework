import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class buildframe{
	
	private JFrame frame;
	
	public buildframe(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    frame.setVisible(true);
	}
	
	public JFrame getframe(){
		return frame;
	}
	
}