import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
 
import java.awt.GridLayout;
import java.awt.event.*;
 
public class informationpanel {
	public informationpanel(boolean working, Students groupinscroll, JComboBox<String> JCstudents){
		if( working == false){
			int contents = JCstudents.getSelectedIndex();
				 student k = groupinscroll.getstudent(contents);
				 int g = k.getnumber();
					    buildframe temp = new buildframe();
					     JFrame info = temp.getframe();
					     JPanel uptodown = new JPanel();
					     info.setLayout( new GridLayout(5,1));
					     JTextField j1 = new JTextField(k.getname(),SwingConstants.CENTER);
					     JTextField j2 = new JTextField(k.getemail(),SwingConstants.CENTER);
					     JTextField j3 = new JTextField("Student No.: "+ g ,SwingConstants.CENTER);
					     JTextField j4 = new JTextField("Tutor:    " + k.gettutor(),SwingConstants.CENTER);
					     System.out.println(k.getresults());
					     JTextArea j5 = new JTextArea(k.getresults());
					     j1.setPreferredSize(new Dimension(120,40));
					     j1.setFont(new Font("Serif", Font.BOLD | Font.BOLD, 22));
					     j2.setFont(new Font("Serif", Font.ITALIC | Font.BOLD, 20));
					     j3.setFont(new Font("Serif", Font.BOLD | Font.BOLD, 14));
					     j4.setFont(new Font("Serif", Font.BOLD | Font.BOLD, 14));
					     j5.setFont(new Font("Serif", Font.BOLD | Font.BOLD, 14));
					     info.add(j1);
					     info.add(j2);
					     info.add(j3);
					     info.add(j4);
					     info.add(j5);
					     info.pack();
	           }
			 }
	public informationpanel(){
		
	}
	public void infopanel2(student k){
		 int g = k.getnumber();
		    buildframe temp = new buildframe();
		     JFrame info = temp.getframe();
		     JPanel uptodown = new JPanel();
		     info.setLayout( new GridLayout(5,1));
		     JTextField j1 = new JTextField(k.getname(),SwingConstants.CENTER);
		     JTextField j2 = new JTextField(k.getemail(),SwingConstants.CENTER);
		     JTextField j3 = new JTextField("Student No.: "+ g ,SwingConstants.CENTER);
		     JTextField j4 = new JTextField("Tutor:    " + k.gettutor(),SwingConstants.CENTER);
		     System.out.println(k.getresults());
		     JTextArea j5 = new JTextArea(k.getresults());
		     j1.setPreferredSize(new Dimension(120,40));
		     j1.setFont(new Font("Serif", Font.BOLD | Font.BOLD, 22));
		     j2.setFont(new Font("Serif", Font.ITALIC | Font.BOLD, 20));
		     j3.setFont(new Font("Serif", Font.BOLD | Font.BOLD, 14));
		     j4.setFont(new Font("Serif", Font.BOLD | Font.BOLD, 14));
		     j5.setFont(new Font("Serif", Font.BOLD | Font.BOLD, 14));
		     info.add(j1);
		     info.add(j2);
		     info.add(j3);
		     info.add(j4);
		     info.add(j5);
		     info.pack();
		}
	}
