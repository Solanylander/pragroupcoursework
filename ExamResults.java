import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import javax.swing.JFileChooser;


public class ExamResults {
	private ArrayList<String> lines;
	private Assessment assess;
public Assessment run(Students group){
	assess = new Assessment();
	lines = new ArrayList<>();
	int size = group.getsize();
	try{
		final JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(null);
		BufferedReader br = null;
		String line = null;
		BufferedReader reader = new BufferedReader(new FileReader(fc.getSelectedFile()));
		while ((line = reader.readLine()) != null) {
		    lines.add(line+",");
		}
		String x = lines.get(0);
		String[] columns = {"#Module","#Ass#","#Cand Key","Mark","Grade"};
		int [] columnnumb = new int[5];
		int c  = 0 ;
		String col = "";
		String nos = "\",";
		for(int i = 0; i < x.length(); i++){
			if (!nos.contains(x.charAt(i)+"")){
				col += x.charAt(i);
			}
			if(x.charAt(i) == nos.charAt(1)){
				
				c++;
				try{
				if (col.equals(columns[0])){
					columnnumb[0] = c;		
				}
				else if (col.equals(columns[1])){
					columnnumb[1] = c;				
				}
				else if (col.equals(columns[2])){
					columnnumb[2] = c;			
				}
				else if (col.equals(columns[3])){
					columnnumb[3] = c;			
				}
				else if (col.equals(columns[4])){
					columnnumb[4] = c;	
				}
				}catch(Exception e){
					
				}
				col = "";
			}
		}

		for (int j = 1; j < lines.size(); j++){
			x = lines.get(j);

			String row = "";
			int d = 0;
			String[] res = new String[5];
			res[4] = "";
			for(int i = 0; i < x.length(); i++){
				if (!nos.contains(x.charAt(i)+"")){
					row += x.charAt(i);
				}
				if(x.charAt(i) == nos.charAt(1)){
					d++;
					try{
					if(d == columnnumb[0]){
						res[0] = row;
					}if(d == columnnumb[1]){
						res[1] = row;
					}if(d == columnnumb[2]){
						res[2] = row;
					}if(d == columnnumb[3]){
						res[3] = row;
					}if(d == columnnumb[4]){
						res[4] = row;
					}
					row = "";

				}
				catch(Exception e){
				}
					
			}
		}
			Result temp = new Result(res[0],res[1],res[2],res[3],res[4]);
			assess.addresult(temp);
	}		
} 
	catch (Exception e) {
		} 
	return assess;
}
}
