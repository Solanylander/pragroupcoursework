import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;


public class TabbedPane extends JFrame{
	private JTabbedPane tabbedPane;
	public TabbedPane(){
	setSize( 300, 400 );

	JPanel topPanel = new JPanel();
	topPanel.setLayout( new BorderLayout() );
	getContentPane().add( topPanel );
	tabbedPane = new JTabbedPane();
	topPanel.add( tabbedPane, BorderLayout.CENTER );
}
	
	
	
public void newPage(Assessment a, final Students b){

	String[] array = new String[a.getResults().size()];
	int i = 0;
			for(int j = 0; j < a.getResults().size(); j++){
			for(int k = 0; k < b.getsize();k++){
			if(Integer.parseInt(a.getResults().get(j).getcand()) == b.getstudent(k).getnumber()){
					array[i] = b.getstudent(k).getname() + "(" + a.getResults().get(j).getmark() + ")";
					i++;
					
			
			}
		}
	}
	final JList  list = new JList(array);
	MouseAdapter mouseListener = new MouseAdapter() {
	    public void mouseClicked(MouseEvent e) {
	        if (e.getClickCount() == 2) {
	        		String selectedItem = (String) list.getSelectedValue();
	        		String clickable = "";
	        		int i = 0;
	        			while(!(""+selectedItem.charAt(i)).equals("(")){
	        				clickable += selectedItem.charAt(i);
	        				i++;
	        			}
	        			System.out.println(clickable);
	        			informationpanel click = new informationpanel();
	        			student h;
	        			for(int p = 0; p < b.getsize(); p++){
	        			 if (clickable.equals(b.getstudent(p).getname())){
	        				 h = b.getstudent(p);
	 	        			click.infopanel2(h);
	        			 }
	        			}
	         }
	    }
	};
	list.addMouseListener(mouseListener);
	JPanel te = new JPanel();
	te.setLayout(new BorderLayout());
	JScrollPane scrollPane = new JScrollPane(list);
	JLabel label = new JLabel(a.getResults().get(0).getmodule() + "  " + a.getResults().get(0).getass());
	te.add(label, BorderLayout.NORTH);
	te.add(scrollPane, BorderLayout.CENTER);
	tabbedPane.addTab( "Page 1", te);
	tabbedPane.add(te);
	
	
	}






}
