import java.io.BufferedReader;
import java.io.FileReader;

import javax.swing.JFileChooser;


public class Read{
	public void run(Students group) {
		int size = group.getsize();
		try{
			final JFileChooser fc = new JFileChooser();
			int returnVal = fc.showOpenDialog(null);
			BufferedReader br = null;
			String line = null;
	 
		try {

    		int found = 0;
    		int notfound = 0;
			br = new BufferedReader(new FileReader(fc.getSelectedFile()));
			while ((line = br.readLine()) != null) {
				String[] Marks = line.split(",");
				boolean markmatch = false;
				 for (int i = 0; i < size; i++) {
					 for (int j = 0; j < size; j++) {
						 student temp = group.getstudent(j);
					 if (Integer.parseInt(Marks[0]) == temp.getnumber()){
						 temp.setmark(Marks[1]);
						 markmatch = true;
						 notfound++;
					 }
				 }
	        	 if (markmatch == false){
	        			 found++;
	        	 }
	        }
		 }
			System.out.println("Anonymous marking codes imported. "
    		+  notfound/size +" codes were for known students; "
			+found/size
    		+" codes were "
    		+  "for unknown students");
	} 
	catch (Exception e) {
	} 
		finally {
			if (br != null) {
				try {
					br.close();
			    } 
				catch (Exception e){
					
				}
			}
		}
	 }
		catch(Exception e){
		 				   	}
	}
}