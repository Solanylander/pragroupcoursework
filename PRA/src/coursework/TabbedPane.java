package coursework;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import org.jfree.data.xy.XYDataset;


public class TabbedPane extends JFrame{
	private int o;
	private JTabbedPane tabbedPane;
	public TabbedPane(){
	setSize( 300, 400 );
	o = 0;
	JPanel topPanel = new JPanel();
	topPanel.setLayout( new BorderLayout() );
	getContentPane().add( topPanel );
	tabbedPane = new JTabbedPane();
	topPanel.add( tabbedPane, BorderLayout.CENTER );
	Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
 	this.setLocation(dim.width/2+this.getSize().width, dim.height/2-this.getSize().height/2);
}
	
	
	
public void newPage(Assessment a, final Students b, JMenuItem CTA){

	final String[] array = new String[a.getResults().size()];
	int i = 0;
	final ArrayList<student> stu = new ArrayList<>();
	final ArrayList<String> mark = new ArrayList<>();
			for(int j = 0; j < a.getResults().size(); j++){
			for(int k = 0; k < b.getsize();k++){
			if(Integer.parseInt(a.getResults().get(j).getcand()) == b.getstudent(k).getnumber()){
					array[i] = b.getstudent(k).getname() + "(" + a.getResults().get(j).getmark() + ")";
					i++;
					stu.add(b.getstudent(k));
					mark.add(a.getResults().get(j).getmark());
			}
		}
	}
	final JList  list = new JList(array);
	listclicker click = new listclicker(b,list);
	JPanel te = new JPanel();
	te.setLayout(new BorderLayout());
	JScrollPane scrollPane = new JScrollPane(list);
	JLabel label = new JLabel(a.getResults().get(0).getmodule() + "  " + a.getResults().get(0).getass());
	te.add(label, BorderLayout.NORTH);
	te.add(scrollPane, BorderLayout.CENTER);
	tabbedPane.addTab( "Page 1", te);
	final int d = o;
	    			o++;
	tabbedPane.add(te);
	CTA.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
	    		try{
	    			if(tabbedPane.getSelectedIndex() == d){
	    			compare scat = new compare();
	    			System.out.println();
	    			 XYSeriesCollection result = new XYSeriesCollection();
	    			  XYSeries series = new XYSeries("Students");
	    			
	    			
	    			for(int h = 0; h < stu.size(); h++){
	    	            scat.compares(stu.get(h), mark.get(h), series);
	    			}
	    			 result.addSeries(series);
	    			 XYDataset a = result;
	    			scat.end(a);
	    	}
	    	}catch(Exception t){
	    		
	    	
	    	}
	    }
	});
	
	}






}
