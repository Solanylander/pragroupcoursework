package coursework;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
 
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.*;
import java.util.ArrayList;
 
public class informationpanel {
	public informationpanel(boolean working, Students groupinscroll, JList JCstudents){
		if( working == false){
			int contents = JCstudents.getSelectedIndex();
				 student k = groupinscroll.getstudent(contents);
				 int g = k.getnumber();
				 	ArrayList<String> results = k.getresults();
					    buildframe temp = new buildframe();
					     JFrame info = temp.getframe();
					     info.setLayout( new GridLayout(4+results.size(),1));
					     JLabel j1 = new JLabel(k.getname(),SwingConstants.CENTER);
					     JLabel j2 = new JLabel(k.getemail(),SwingConstants.CENTER);
					     JLabel j3 = new JLabel("Student No.: "+ g ,SwingConstants.CENTER);
					     JLabel j4 = new JLabel("Tutor:    " + k.gettutor(),SwingConstants.CENTER);
					     j1.setPreferredSize(new Dimension(120,40));
					     j1.setFont(new Font("Serif", Font.BOLD | Font.BOLD, 22));
					     j2.setFont(new Font("Serif", Font.ITALIC | Font.BOLD, 20));
					     j3.setFont(new Font("Serif", Font.BOLD | Font.BOLD, 14));
					     j4.setFont(new Font("Serif", Font.BOLD | Font.BOLD, 14));
					     info.add(j1);
					     info.add(j2);
					     info.add(j3);
					     info.add(j4);
					     for (int i = 0; i < results.size();i++)
					     {
						     JLabel j5 = new JLabel(results.get(i).substring(1),SwingConstants.CENTER);
						     j5.setFont(new Font("Serif", Font.BOLD | Font.BOLD, 14));
						     info.add(j5);
						 }
					     	Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
					     	info.setLocation(dim.width/2-info.getSize().width/2, dim.height/2-info.getSize().height/2);
					info.pack();
	           		}
				}
	public informationpanel()
	{
		
	}
	public void infopanel2(student k){
		 int g = k.getnumber();
		 	ArrayList<String> results = k.getresults();
		    buildframe temp = new buildframe();
		     JFrame info = temp.getframe();
		     info.setLayout( new GridLayout(4+results.size(),1));
		     JLabel j1 = new JLabel(k.getname(),SwingConstants.CENTER);
		     JLabel j2 = new JLabel(k.getemail(),SwingConstants.CENTER);
		     JLabel j3 = new JLabel("Student No.: "+ g ,SwingConstants.CENTER);
		     JLabel j4 = new JLabel("Tutor:    " + k.gettutor(),SwingConstants.CENTER);
		     j1.setPreferredSize(new Dimension(120,40));
		     j1.setFont(new Font("Serif", Font.BOLD | Font.BOLD, 22));
		     j2.setFont(new Font("Serif", Font.ITALIC | Font.BOLD, 20));
		     j3.setFont(new Font("Serif", Font.BOLD | Font.BOLD, 14));
		     j4.setFont(new Font("Serif", Font.BOLD | Font.BOLD, 14));
		     info.add(j1);
		     info.add(j2);
		     info.add(j3);
		     info.add(j4);    
		     	for (int i = 0; i < results.size();i++){
		     		JLabel j5 = new JLabel(results.get(i).substring(1),SwingConstants.CENTER);
		     		j5.setFont(new Font("Serif", Font.BOLD | Font.BOLD, 14));
		     		info.add(j5);
		     	}
		     	Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		     	info.setLocation(dim.width/2-info.getSize().width/2, dim.height/2-info.getSize().height/2);
		     info.pack();
		}
	}
