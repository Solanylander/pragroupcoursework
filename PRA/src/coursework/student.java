package coursework;
import java.util.ArrayList;


public class student {
	
private String name;
private int number;
private String email;
private String tutor;
private ArrayList<String> markingcodes;
private ArrayList<String> results;
private String mainemail;
	public student(String n, int x, String e, String z)
	{
		mainemail = "";
		name = n;
		number = x;
		email = e;
		tutor = z;
		markingcodes =new ArrayList<>();
		results = new ArrayList<>();
	}
	
	public String getname()
	{
		String ret = name;
		return ret;
	}
	public int getnumber()
	{
		int ret = number;
		return ret;
	}
	public String getemail()
	{
		String ret = email;
		return ret;
	}
	public String gettutor()
	{
		String ret = tutor;
		return ret;
	}
	public String getmark(int i)
	{
		String ret = markingcodes.get(i);
		return ret;
	}
	public void setmark(String mark)
	{
		markingcodes.add(mark);

	}
	public void addgrade(String a, String b, String c)
	{
		results.add(a+" "+b+":  "+c);
	}
	public int marklength(){
		return markingcodes.size();
	}
	public ArrayList<String> getresults()
	{
		return results;
	}
	public void setemail(String a){
		mainemail += a;
	}
	public String getemailmain(){
		return mainemail;
	}
}
