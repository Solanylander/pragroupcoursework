package coursework;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;


public class main implements ActionListener {
	   static JList list;
	   private static JTextField TextInput;
	   static Students group;
	   static Students groupinscroll = new Students();
	   static JScrollPane scrollPane;
	   static JFrame frame;
	   static JPanel west;
	   static boolean working = false;
	   public static void main(String[] args) {
		     buildframe temp = new buildframe();
		     frame = temp.getframe();
	    	 JMenuBar bar = new JMenuBar();
	    	 group = new Students();
	         JLabel JLtop = new JLabel("                             "
	         		+ "         PRA Coursework - Alpha Evolution");
	           frame.setJMenuBar(bar);
	           frame.setLayout(new BorderLayout());
	           frame.setMinimumSize(new Dimension(640 ,480));
	       	west = new JPanel();
	       	frame.setLayout(new BorderLayout());
	    	frame.add(west, BorderLayout.WEST);
	    	west.setLayout( new BoxLayout(west, BoxLayout.PAGE_AXIS));
	    	TextInput = new JTextField();
	    	TextInput.getDocument().addDocumentListener(CSVreader);
	    	JMenu File = new JMenu("File");;
	    	JMenu Data = new JMenu("Data");
	    	Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	    	frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
	    	final JMenuItem LAMC = new JMenuItem("Load anonymous marking codes");
	    	final JMenuItem LER = new JMenuItem("Load exam results");
	    	final JMenuItem CTA = new JMenuItem("Compare to Average");
	    	final JMenuItem EMAIL = new JMenuItem("Email to Students");
	    	final JMenuItem EMAILSetting = new JMenuItem("Email Settings");
	    	final JMenuItem Fetch = new JMenuItem("Fetch Participation");
	    	bar.add(File);
	        bar.add(Data);
	        bar.add(JLtop);
	    	File.add(LAMC);
	    	File.add(LER);
	    	Data.add(CTA);
	    	Data.add(EMAIL);
	    	Data.add(EMAILSetting);
	    	Data.add(Fetch);
	    	EMAILSetting.addActionListener(new ActionListener() {
			    public void actionPerformed(ActionEvent e) {
			    	
			    	try {
						Emailset settings = new Emailset();
					} catch (IOException e1) {
					}
			    }
	    	});
	    	
	    	EMAIL.addActionListener(new ActionListener() {
			    public void actionPerformed(ActionEvent e) {
			    	
			    	Email mail = new Email(group);
			    }
	    	});
	    	Fetch.addActionListener(new ActionListener() {
			    public void actionPerformed(ActionEvent e) {
			    	System.out.print("hi");
			    Fetch fetch = new Fetch();
			    }
	    	});
	    	 west.setMaximumSize(new Dimension(10,100));
	    	 TextInput.setMaximumSize(new Dimension(200,100));
	    	 west.add(TextInput);
		     TestStudentData g = new TestStudentData();
		     group = g.Test();
		     groupinscroll = g.Test();
		     int size = group.getsize();
		     
		     String[] scroll = new String[group.getsize()];
		     
		     
	         for(int i = 0; i < size; i++){
	         student test = group.getstudent(i);
	         scroll[i] = test.getname() + "(" + test.getnumber() + ")";
	         }
	         
	         list = new JList(scroll);
	    	scrollPane = new JScrollPane(list);
	    	west.add(scrollPane);
	    	listclicker click = new listclicker(group,list);
	    	
	    	frame.pack();
	         
		LAMC.addActionListener(new ActionListener() {

		    public void actionPerformed(ActionEvent e) {
		        Read obj = new Read();
		        obj.run(group,frame);
		    }
		});
		LER.addActionListener(new ActionListener() {
			TabbedPane mainFrame	= new TabbedPane();
		    public void actionPerformed(ActionEvent e) {
		    	try{
		        ExamResults obj = new ExamResults();
		        Assessment assess = obj.run(group);
		        for(int i = 0; i < assess.getResults().size(); i++){
		        	String a = assess.getResults().get(i).getcand().substring(1);
		        	for(int j = 0; j < group.getsize(); j++){
		        		for(int g = 0; g < group.getstudent(j).marklength(); g++){
		        		String b = group.getstudent(j).getmark(g);
		        			if(a.equals(b)){
		        				assess.getResults().get(i).setcand(group.getstudent(i).getnumber()+"");

				        		group.getstudent(j).addgrade(assess.getResults().get(i).getmodule(), assess.getResults().get(i).getass(), assess.getResults().get(i).getmark());
		        				}
		        	}
		        	}
		        	if(assess.getResults().get(i).getcand().contains("/")){
		        		int p = 1;
		        		String temp = "";
		        		String move = assess.getResults().get(i).getcand();
		        		while (!((move.charAt(p)+"").equals("/"))){
		        			temp += move.charAt(p);
		        			p++;

				    	
		        		}
		        		assess.getResults().get(i).setcand(temp);
		        		for(int j = 0; j < group.getsize(); j++){
		        			if (temp.equals(group.getstudent(j).getnumber()+"")){
		        		group.getstudent(j).addgrade(assess.getResults().get(i).getmodule(), assess.getResults().get(i).getass(), assess.getResults().get(i).getmark());
		        			}
		        		}
		        	}
		        	}

			if ((assess.getResults().get(0).getass().charAt(0)+"").equals("#")){
				mainFrame.setVisible( true );
				mainFrame.newPage(assess,group,CTA);
		    	}
		    	}
		    	catch(Exception e1){

		    	}        
		    	groupsearch();
		    }
		});
	    
		
		TextInput.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				informationpanel info = new informationpanel(working, groupinscroll, list);
			}
		});
  }
	   

	   
	   static DocumentListener CSVreader = new DocumentListener() {
		   
		public void changedUpdate(DocumentEvent arg0) {
			
		}

		public void insertUpdate(DocumentEvent arg0) {
			 groupsearch();
		}

		public void removeUpdate(DocumentEvent arg0) {
			 groupsearch();
		}
		   
	};
	   
	public void actionPerformed(ActionEvent arg0) {
		
	}

	public static void groupsearch() {
		String s = TextInput.getText();
		working = true;
		groupinscroll.clear();
		list.removeAll();
        	for(int i = 0; i < group.getsize(); i++){
        		student test = group.getstudent(i);
        		String t = test.getnumber() + "";
        		if(test.getname().toLowerCase().contains(s.toLowerCase()) || t.contains(s)){
        			groupinscroll.addstudent(test);
        		}

   		   
        	}
        	  String[] scroll = new String[groupinscroll.getsize()];
       		  for(int i = 0; i < groupinscroll.getsize(); i++){
     	         student test = groupinscroll.getstudent(i);
     	         scroll[i] = test.getname() + "(" + test.getnumber() + ")";
     	         }
       		  west.remove(scrollPane);
       		  list = new JList(scroll);
  	    	scrollPane = new JScrollPane(list);

	    	 list.setMaximumSize(new Dimension(100,100));
  	    	west.add(scrollPane);
  	    	listclicker click = new listclicker(groupinscroll,list);
  	    	frame.pack();
        working = false;
	}
}

 
