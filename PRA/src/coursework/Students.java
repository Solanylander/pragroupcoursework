package coursework;
import java.util.ArrayList;

public class Students {
	
private ArrayList<student> kclstudents;

	public Students() {
		kclstudents = new ArrayList<>();
	}
	
	public void addstudent(student x){
		kclstudents.add(x);
	}

	public int getsize(){
		return kclstudents.size();
	}

	public student getstudent(int i){
		student k = kclstudents.get(i);
		return k;
	}

	public void clear() {
		kclstudents.clear();
	}
}
