package coursework;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;


public class Email {
private JFrame frame;
private JScrollPane scrollpane;
private ArrayList<JCheckBox> buttons;
private ArrayList<student> checked;
	public Email(final Students group){
		checked = new ArrayList<>();
		buildframe f = new buildframe();
		buttons = new ArrayList<>();
		frame = f.getframe();
		frame.setLayout(new BorderLayout());
		email2(group, frame);
	}
		
	public void email2(final Students group, final JFrame frame){	
		final JPanel check = new JPanel();
    	check.setLayout( new BoxLayout(check, BoxLayout.PAGE_AXIS));
    	final JCheckBox all = new JCheckBox("Select All");
		check.add(all);
		final JCheckBox none = new JCheckBox("Select None");
		check.add(none);
		all.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	none.setSelected(false);
		    	for (int j = 0; j < buttons.size(); j++){
		    		buttons.get(j).setSelected(true);
		    		final student temp = group.getstudent(j);
		    		checked.add(temp);
		    	}
		    }
		});
		none.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	all.setSelected(true);
		    	for (int j = 0; j < buttons.size(); j++){
		    		buttons.get(j).setSelected(false);
		    		final student temp = group.getstudent(j);
		    		checked.remove(temp);
		    	}
		    }
		});
		
	for(int i = 0; i < group.getsize(); i++){
		final student temp = group.getstudent(i);
			final JCheckBox box = new JCheckBox(temp.getname());
			buttons.add(box);
			check.add(box);
			box.addActionListener(new ActionListener() {
			    public void actionPerformed(ActionEvent e) {
			    	if(box.isSelected()){
			    		checked.add(temp);
			    	}else{
			    		checked.remove(temp);
			 
			    	}
			    }
			});
		}
  	JLabel lab = new JLabel("  ");
  	check.add(lab);
  	scrollpane = new JScrollPane(check);
	frame.add(scrollpane, BorderLayout.WEST);
	final JPanel headfoot = new JPanel();
	final JTextField Header = new JTextField("");
	final JTextField  Footer = new JTextField("");
	headfoot.setLayout( new BoxLayout(headfoot, BoxLayout.PAGE_AXIS));
	frame.add(headfoot, BorderLayout.EAST);
	frame.add(new JLabel("                      "), BorderLayout.CENTER);
	Header.setMaximumSize(new Dimension(300,100));
	headfoot.add(new JLabel("                  Header"));
	headfoot.add(Header);
	headfoot.add(Box.createRigidArea(new Dimension(250,250)));
	headfoot.add(new JLabel("                  Footer"));
	headfoot.add(Footer);
	frame.setSize(new Dimension(600, 400));
  	JButton next = new JButton("Next");
  	headfoot.add(next);
  	next.setAlignmentX(-400);
  	next.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
	    	   frame.remove(scrollpane);
	    	   frame.remove(headfoot);
	    	Emailpreview email = new Emailpreview(frame,checked,Header.getText(),Footer.getText(), group);
	    }
	});
	}
}
