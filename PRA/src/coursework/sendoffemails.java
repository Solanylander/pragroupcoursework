package coursework;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
public class sendoffemails{
	private String username;
	final private String password;
	private String Host;
	private String port;
	final private String Header;
	final private ArrayList<student> emaillist;
	public sendoffemails(String passwords, ArrayList<student> checked, String head) throws IOException{
		password = passwords;
		emaillist = checked;
		Header = "head";
		
		BufferedReader br = null;
		String[] settings = new String[4];
		int i = 0;
			String sCurrentLine;
			br = new BufferedReader(new FileReader("settings.ini"));
			while ((sCurrentLine = br.readLine()) != null) {
				settings[i] = sCurrentLine.substring(1);
				i++;
			}
				if (br != null)br.close();
				Host = settings[1];
				username = settings[0];
				port = settings[2];
				sending();
	}
		 public void sending(){
		
 
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", Host);
		props.put("mail.smtp.port", port);
 
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });
 
		try {
			System.out.println(username + "\n" + password);
			for(int yo = 0; yo < emaillist.size(); yo++){
    			student temp = emaillist.get(yo);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(temp.getemail()));
			message.addRecipients(Message.RecipientType.TO,
					InternetAddress.parse(username));
			message.setSubject(Header);
			message.setText(temp.getemailmain());
 
			Transport.send(message);
			JFrame frame = new JFrame();
			JOptionPane.showMessageDialog(frame, "Sent");
			frame.dispose();
			}
		} catch (MessagingException e) {
			JFrame frame = new JFrame();
			JOptionPane.showMessageDialog(frame, "Not Sent");
			frame.dispose();
		}
	}
}
